<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel 7 Vue CRUD
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-vue-crud.git`
2. Go inside the folder: `cd laravel-7-vue-crud`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000/vue

### Screen shot
Home Page

![Home Page](img/home.png "Home Page")

Create New Article Page

![Create New Article Page](img/create.png "Create New Article Page")

Show Details Page

![Show Details Page](img/details.png "Show Details Page")

Edit Article Page

![Edit Article Page](img/edit.png "Edit Article Page")

Delete Article Page

![Delete Article Page](img/delete.png "Delete Article Page")
